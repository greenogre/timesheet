#!/bin/bash

CATEGORIES=(Research "Planning/Design" Programming Testing Meetings)
WEIGHTS=(75 147 342 408 19) # Weight = Percent time spent * 10
PROJECTS=("PULSE" "PULSE" "PULSE" "PULSE" "PULSE" )

EMPLOYEE_ID="DGIBSON"
# PROJECT_ID="PULSE"
HOURS_DAY=7.5               # multiple of 0.5 hours

## Ontario Public Holiday Tables
# Easter Sunday
EASTER_SUNDAY="
2013/03/31\n
2014/04/20\n
2015/04/05\n
2016/03/27\n
2017/04/16\n
2018/04/01\n
2019/04/21\n
2020/04/12\n
2021/04/04\n
"

## Ontario Public Holidays (table deprecated)
PUBLIC_HOLIDAYS="
2016/01/01\n
2016/02/15\n
2016/03/25\n
2016/05/23\n
2016/07/01\n
2016/08/01\n
2016/09/05\n
2016/10/10\n
2016/12/25\n
2016/12/26\n
2017/01/01\n
2017/02/20\n
2017/04/14\n
2017/05/22\n
2017/07/03\n
2017/08/07\n
2017/090/4\n
2017/10/09\n
2017/12/25\n
2017/12/26\n
2018/01/01\n
2018/02/19\n
2018/03/30\n
2018/05/21\n
2018/07/01\n
2018/08/06\n
2018/09/03\n
2018/10/08\n
2018/12/25\n
2018/12/26\n
2019/01/01\n
2019/02/18\n
2019/04/19\n
2019/05/20\n
2019/07/01\n
2019/08/05\n
2019/09/02\n
2019/10/14\n
2019/12/25\n
2019/12/26\n
"
DEBUG=0

SCRIPTNAME=$(basename $0)
SCRIPTPATH=$(dirname $0)
VERSION="0.6.0"
LICENSE="GPL v3"
COPYRIGHT="D. Gibson <dgibson@rlsolutions.com>, October 2016"


function show_help {
  printf "\n${SCRIPTNAME} : Format timesheets for external systems.\n"
  printf "Usage:\n"
  printf "\t${SCRIPTNAME} [-m|-y|-f|-h|-d|-V]\n"
  printf "\nOptions:\n"
  printf "\t-m <month>\n"
  printf "\t\t - Month to display. Defaults to month prior to current month\n"
  printf "\t\t   Valid values: [1-12], full month name or 3 letter month abbreviation (e.g. \"Nov\")\n"
  printf "\t-y [<yy>|<ccyy>]\n"
  printf "\t\t - Year of month to display.\n"
  printf "\t\t   Defaults to current century/year.\n"
  printf "\t-g\t - Include Government and Bank Holidays.\n"
  printf "\t-f [csv|tty]\n"
  printf "\t\t - Output format.\n"
  printf "\t\t     \"tty\" for human-readable terminal output.\n"
  printf "\t\t     \"csv\" for Comma Seperated Value output.\n"
  printf "\t\t   Defaults to \"tty\".\n"
  printf "\t-d <integer>\n"
  printf "\t\t - Set debug level. Verbosity increases with value.\n"
  printf "\t-V\t - Print version and exit.\n"
  printf "\t-h\t - Show this help message.\n\n"
}

function get_category {
  local VALUE=${1}
  for CAT in $(seq 0 ${NUM_CATS}); do
    if [ ${VALUE} -lt ${PERCENT_TABLE[$CAT]} ]; then
      printf "${CAT}"
      return 0
    fi
  done
  return 1
}

function print_categories {
  local VALUES=($@)
  local HOURS=0
  local TOTAL_HOURS=0
  
  for CAT in $(seq 0 ${NUM_CATS}); do
    HOURS=$(printf "${VALUES[${CAT}]} 2" | awk '{printf "%5.2f \n", $1/$2}')
    TOTAL_HOURS=$(expr ${TOTAL_HOURS} + ${VALUES[${CAT}]})
    printf "\t ${HOURS}\t"
  done
  printf "\t"
  print_hours ${TOTAL_HOURS}
  printf "\n"
}

function print_csv_categories {
  local VALUES=($@)
  local HOURS=0
  local CSV_DATE=$(date -d "${START_MONDAY} + ${DAY_OFFSET} day" +"%Y-%m-%d")
  local COST_CAT_ID=
  local PROJECT_ID=
  
  for CAT in $(seq 0 ${NUM_CATS}); do
    COST_CAT_ID="${CATEGORIES[${CAT}]}"
    PROJECT_ID="${PROJECTS[${CAT}]}"
    HALF_HOURS=${VALUES[${CAT}]}
    HOURS=$(printf "${HALF_HOURS} 2" | awk '{printf "%.2f\n", $1/$2}')
    if [ ${HALF_HOURS} -gt 0 ]; then
      printf "\"${EMPLOYEE_ID}\",\"${CSV_DATE}\",\"${PROJECT_ID}\",\"${COST_CAT_ID}\",${HOURS},\n"
    fi
  done
}

function print_hours {
  local VALUE="${1}"
  local HOURS=0
  
  HOURS=$(printf "${VALUE} 2" | awk '{printf "%6.2f \n", $1/$2}')
  printf "${HOURS}"
}


function sum_categories {
  local VALUES=($@)
  local TOTAL=0
  for CAT in $(seq 0 ${NUM_CATS}); do
    TOTAL=$(expr ${TOTAL} + ${VALUES[${CAT}]})
  done
  printf "${TOTAL}\n"
}

function print_headers {
  local WEEK="${1}"
  local WEEK_STRING1="Month"
  local WEEK_STRING2="    "
  if [ -n "${WEEK}" ]; then
    #if [ ${NO_HOLS} -eq 0 ]; then
      WEEK_STRING1="Day  "
      WEEK_STRING2="   Hol"
    #fi
    printf "Week $(expr ${WEEK} + 1):\n"
  else
    printf "${YEAR}:\n"
  fi
  print_seperator
  printf "ProjectID ->"
  for CAT in $(seq 0 ${NUM_CATS}); do
    print_field "${PROJECTS[${CAT}]}                "
  done
  printf "\n"
  printf "${WEEK_STRING1} ${WEEK_STRING2}"
  for CAT in $(seq 0 ${NUM_CATS}); do
    print_field "${CATEGORIES[${CAT}]}                "
  done
  print_field "Total                 "
  printf "\n"
}

function print_csv_headers {
  printf "\"employee id\",\"work_date (YYYY-mm-dd)\",\"project_id\",\"cost_cat_id\",\"hours\",\"comment\"\n"
}

function print_field {
  local FIELD="${1}"
  local F_LEN=${2}
  
  if [ -z "${F_LEN}" ]; then
    F_LEN=15
  fi
  
  if [ ${#FIELD} -gt ${F_LEN} ]; then
    FIELD=${FIELD:0:${F_LEN}}
  fi
  
  printf "\t%${F_LEN}s" "${FIELD}"
}

function print_seperator {
  local SEP_NUM=${1}
  local SEP_CHAR=${2}
  local SEP_LEN=16
  
  # Default seperator values
  if [ -z "${SEP_NUM}" ]; then
    SEP_NUM=$(expr ${#CATEGORIES[@]} + 2)
  fi
  if [ -z "${SEP_CHAR}" ]; then
    SEP_CHAR="-"
  fi
  SEP_LEN=$(expr ${SEP_NUM} \* 16)
  
  printf -v sep '%*s' ${SEP_LEN} ; echo "${sep// /${SEP_CHAR}}"
}

function wipe_month_total {
  for CAT in $(seq 0 ${NUM_CATS}); do
    MONTH_TOTAL[${CAT}]=0
  done
}

function sum_month_total {
  for CAT in $(seq 0 ${NUM_CATS}); do
    MONTH_TOTAL[${CAT}]=$(expr ${MONTH_TOTAL[${CAT}]} + ${WEEK_TOTAL[${CAT}]})
  done
}

function wipe_week_total {
  for CAT in $(seq 0 ${NUM_CATS}); do
    WEEK_TOTAL[${CAT}]=0
  done
}

function sum_week_total {
  for CAT in $(seq 0 ${NUM_CATS}); do
    WEEK_TOTAL[${CAT}]=$(expr ${WEEK_TOTAL[${CAT}]} + ${DAY_TOTAL[${CAT}]})
  done
}

function sum_day_total {
  local TODAY_TOTAL=
  for CAT in $(seq 0 ${NUM_CATS}); do
    TODAY_TOTAL=$(expr ${TODAY_TOTAL} + ${DAY_TOTAL[${CAT}]})
  done
  printf "${TODAY_TOTAL}"
}

function wipe_day_total {
  for CAT in $(seq 0 ${NUM_CATS}); do
    DAY_TOTAL[${CAT}]=0
  done
}

function check_month {
  local MENSE="${1}"
  local MENSIS="January February March April May June July August September October November"
  local MEN="Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec"
  
  if [ -n "${MENSE}" ]; then
    if `printf "${MENSIS} ${MEN}\n" | grep -qwi "${MENSE}"`; then
      printf "$(date -d "${MENSE} 1" +%m)"
      return 0
    elif [ ${MENSE} -ge 1 ] && [ ${MENSE} -le 12 ]; then
      printf "$(date -d "${MENSE}/1" +%m)"
      return 0
    fi
  fi
  printf "\nInvalid month \"${MENSE}\".\n" 1>&2
  show_help 1>&2
  return 1
}

function check_year {
  local ANNUS=${1}
  
  if [[ "${ANNUS}" =~ [0-9][0-9][0-9][0-9] ]] || [[ "${ANNUS}" =~ [0-9][0-9] ]]; then
    case ${#ANNUS} in
      2) printf "20${ANNUS}"
         return 0
         ;;
      4) printf "${ANNUS}"
         return 0
         ;;
      *) printf "\nInvalid year \"${ANNUS}\".\n" 1>&2
         show_help 1>&2
         return 1
         ;;
    esac
  fi
  printf "\nInvalid year \"${ANNUS}\".\n" 1>&2
  show_help 1>&2
  return 1
}

function check_holiday {
  local TEST_DAY="${1}"
  local TEST_MONTH=$(date -d "${TEST_DAY}" +"%m")
  local TEST_YEAR=$(date -d "${TEST_DAY}" +"%Y")
  local MONTH_DAY=$(date -d "${TEST_DAY}" +"%d")
  local WEEK_DAY=$(date -d "${TEST_DAY}" +"%u")
  
  ## Ontario Public Holidays
  # 1st Jan, New Years Day
  # 3rd Monday in Feb, Family Day
  # Friday before Easter Sunday, Good Friday
  # Monday after Easter Sunday, Easter Monday (GOVERNMENT ONLY)
  # Monday on or before 24th May, Victoria Day
  # 1st July, Canada Day
  # 1st Monday in August, Civic Day
  # 1st Monday in September, Labour Day
  # 2nd Monday in October, Thanksgiving
  # 11th November, Remembrance Day (GOVERNMENT ONLY)
  # 25th December, Christmas Day and
  # 26th December, Boxing Day
  
  EASTER=$(printf "${EASTER_SUNDAY}" | grep "${TEST_YEAR}")
  
  # Return 0 if holiday, otherwise return 1
  
  # New Years Day, Canada Day, Christmas Day and Boxing Day (fixed, with carry)
  if [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/0[17]\/01$ ]] || \
     [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/12\/2[56]$ ]]; then
    if [ ${WEEK_DAY} -le 5 ]; then
      return 0
    else
      PH_CARRY=$(expr ${PH_CARRY} + 1)
      return 1
    fi
  # Family Day, Victoria Day, Civic Day, Labour Day or Thanksgiving
  # ( <n>th Monday in month)
  # First test if in range for nth Monday in month
  elif [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/02\/1[5-9]$ ]] || \
       [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/02\/2[0-1]$ ]] || \
       [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/05\/1[8-9]$ ]] || \
       [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/05\/2[0-4]$ ]] || \
       [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/0[89]\/0[1-7]$ ]] || \
       [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/10\/0[8-9]$ ]] || \
       [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/10\/1[0-4]$ ]]; then
    # then if it's a Monday in the range, it's a holiday
    if [ ${WEEK_DAY} -eq 1 ]; then
      return 0
    else
      return 1
    fi
  # Good Friday (lookup in Easter Sunday var)
  elif [ "${TEST_DAY}" == "$(date -d "${EASTER} - 2 day" +"%Y/%m/%d")" ]; then
    return 0
  # GOVERNMENT/BANK Only 
  elif [ ${GOV_HOLS} -gt 0 ]; then
    # Easter Monday (lookup in Easter Sunday var)
    if [ "${TEST_DAY}" == "$(date -d "${EASTER} + 1 day" +"%Y/%m/%d")" ]; then
      return 0
    # Remembrance Day (fixed, with carry)
    elif [[ "${TEST_DAY}" =~ ^[[:digit:]]{4}\/11\/11$ ]]; then
      if [ ${WEEK_DAY} -le 5 ]; then
        return 0
      else
        PH_CARRY=$(expr ${PH_CARRY} + 1)
        return 1
      fi
    else
      return 1
    fi
  # otherwise, not a holiday
  else
    return 1
  fi
}

function check_of {
  local F_OUT="${1}"
  local FORMATS="tty csv"
  
  if [ -n "${F_OUT}" ]; then
    if `printf "${FORMATS}\n" | grep -qwi "${F_OUT}"`; then
      printf "${F_OUT}"
      return 0
    fi
  fi
  printf "\nInvalid output format \"${F_OUT}\".\n" 1>&2
  show_help 1>&2
  return 1
}

# Set defaults
YEAR_NOW=$(date +%Y)
USER_YEAR=
MONTH_NOW=$(date +%m)
USER_MONTH=
OF="tty"
GOV_HOLS=0
NO_HOLS=0

while getopts "h?m:y:f:gd:V" opt; do
  case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    m)  USER_MONTH=$(check_month ${OPTARG}) || exit 1
        ;;
    y)  USER_YEAR=$(check_year ${OPTARG}) || exit 1
        ;;
    f)  USER_OF=$(check_of ${OPTARG}) || exit 1
        ;;
    g)  GOV_HOLS=1
        ;;
    d)  DEBUG="${OPTARG}"
        if [ $DEBUG -gt 1 ]; then
          set -x
        fi
        ;;
    V)  printf "\n\tProgram    : ${SCRIPTNAME}\n\t           : Format timesheets for external systems.\n\tVersion    : v${VERSION}\n\tLicense    : ${LICENSE}\n\tCoypright  : ${COPYRIGHT}\n"
        exit 0
        ;;
  esac
done

# Validate params
if [ -n "${USER_YEAR}" ]; then
  YEAR=${USER_YEAR}
else
  YEAR=$(date +%Y)
fi

if [ -n "${USER_MONTH}" ]; then
  MONTH=${USER_MONTH}
else
  MONTH_NOW=$(date +"%m")
  if [ ${MONTH_NOW} -gt 1 ]; then
    MONTH=$(expr ${MONTH_NOW} - 1)
  else
    MONTH=12
    YEAR=$(expr ${YEAR} - 1)
  fi
fi
if [ -n "${USER_OF}" ] ; then
  OF="${USER_OF}"
else 
  OF="tty"
fi

if ! `printf "${EASTER_SUNDAY}" | grep -q "^${YEAR}"`; then
  NO_HOLS=1
  printf "\n*** WARNING ***\n\tEaster Holidays are not known for year ${YEAR}\n\tand will be included as regular days worked.\n" 1>&2
fi


# Begin
DAY_ONE=$(date -d "${YEAR}/${MONTH}/1" +"%u")
PH_CARRY=0
if [ ${DAY_ONE} -le 5 ]; then
  START_MONDAY=$(date -d "${YEAR}/${MONTH}/1 - ${DAY_ONE} day + 1 day" +"%Y/%m/%d")
else
  START_MONDAY=$(date -d "${YEAR}/${MONTH}/1 - ${DAY_ONE} day + 8 day" +"%Y/%m/%d")
  # If 1st or 2nd of month is on a weekend and a holiday,
  # set PH_CARRY to move holiday(s) to beginning of week 
  for D in $(seq 1 ${DAY_ONE} ); do
    check_holiday "$(date -d "${YEAR}/${MONTH}/${D}" +"%Y/%m/%d")"
  done
fi
HALFHOURS_DAY=$(printf "${HOURS_DAY} 2\n" | awk '{printf "%.0f \n", $1*$2}')

NUM_CATS=$(expr ${#CATEGORIES[@]} - 1)

# Generate category lookup array
PERCENT_TOTAL=0
for CAT in $(seq 0 ${NUM_CATS}); do
  PERCENT_TABLE[${CAT}]=$(expr 32 \* ${WEIGHTS[${CAT}]} + ${PERCENT_TOTAL})
  PERCENT_TOTAL=${PERCENT_TABLE[${CAT}]}
done

wipe_month_total

if [ "X${OF}" == 'Xcsv' ]; then
  print_csv_headers
else
  printf "\n\n$(date -d "${YEAR}/${MONTH}/1" +"%B") ${YEAR}:\n\n"
fi

for WEEK in $(seq 0 4); do
  wipe_week_total
  if [ "x${OF}" != 'xcsv' ]; then
    print_headers "${WEEK}"
    print_seperator 
  fi
  for DAY in $(seq 1 7); do
    wipe_day_total
    DAY_OFFSET=$(expr ${WEEK} \* 7 + ${DAY} - 1)
    THIS_DAY=$(date -d "${START_MONDAY} + ${DAY_OFFSET} day" +"%u")
    THIS_MONTH=$(date -d "${START_MONDAY} + ${DAY_OFFSET} day" +"%m")
    THIS_DATE=$(date -d "${START_MONDAY} + ${DAY_OFFSET} day" +"%Y/%m/%d")
    [ $DEBUG -gt 1 ] && printf "THIS_DATE=${THIS_DATE}\n" 1>&2
    # Is this day a public holiday?
    if check_holiday "${THIS_DATE}"; then
      PUB_HOL=1
      PH_STRING='*'
    elif [ ${THIS_DAY} -eq 1 -o ${THIS_DAY} -eq 2 ] && [ ${PH_CARRY} -gt 0 ]; then
      PUB_HOL=1
      PH_STRING='*'
      PH_CARRY=$(expr ${PH_CARRY} - 1)
    else
      PUB_HOL=0
      PH_STRING=' '
    fi
    # Generate day values
    TODAYS_TOTAL=-1
    # Ensure total not greater than day length
    while [ ${TODAYS_TOTAL} -lt 0 ] || [ ${TODAYS_TOTAL} -gt ${HALFHOURS_DAY} ]; do
      for PERIOD in $(seq 1 ${HALFHOURS_DAY}); do
        # If this day is in this month and not on the weekend, generate a value
        # If not, set to max so time is not allocated
        if [ ${THIS_MONTH} -eq ${MONTH} ] && [ ${THIS_DAY} -lt 6 ] && [ $PUB_HOL -eq 0 ]; then
          i=$RANDOM
        else
          i=32768
        fi
        TCAT=$(get_category ${i})
        ERR=$?
        if [ ${ERR} -eq 0 ]; then
          DAY_TOTAL[${TCAT}]=$(expr ${DAY_TOTAL[${TCAT}]} + 1)
        fi
      done
      TODAYS_TOTAL=$(sum_day_total)
    done
    sum_week_total
    if [ "x${OF}" != 'xcsv' ]; then
      printf "$(date -d "${START_MONDAY} + ${DAY_OFFSET} day" +"%a %_d")    ${PH_STRING}"
      print_categories ${DAY_TOTAL[*]}
    else
      print_csv_categories ${DAY_TOTAL[*]}
    fi
  done
  if [ "x${OF}" != 'xcsv' ]; then
    print_seperator 
    printf "Total:  "
    print_categories ${WEEK_TOTAL[*]}
    WEEK_HOURS=$(sum_categories ${WEEK_TOTAL[*]})
    sum_month_total
    print_seperator 
    printf "\n\n"
  fi
done

if [ "x${OF}" != 'xcsv' ]; then
  print_headers
  print_seperator 
  MNTH="$(date -d "${MONTH}/1" +"%B")          "
  printf "${MNTH:0:12}"
  print_categories ${MONTH_TOTAL[*]}
  MONTH_HOURS=$(sum_categories ${MONTH_TOTAL[*]})
  printf "\n"
fi
 
